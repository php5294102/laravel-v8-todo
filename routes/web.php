<?php

use App\Http\Controllers\DataProcessController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoListController;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [TodoListController::class, 'index']);

Route::post('/saveItem', [TodoListController::class, 'saveItem'])->name('saveItemRoute');

Route::post('/markComplete/{id}', [TodoListController::class, 'markComplete'])->name('markCompleteRoute');

Route::get('/test', [TestController::class, 'test']);

Route::get('/isp_mapping', [TestController::class, 'isp_mapping']);

Route::get('/data_process', [DataProcessController::class, 'domain']);
