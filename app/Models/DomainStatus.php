<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DomainStatus extends Model
{
    protected $table = 'domain_status';
    use HasFactory;
}
