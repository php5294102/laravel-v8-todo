<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ListItem;
use Illuminate\Support\Facades\Log;

class TodoListController extends Controller
{
    public function index() {
        // return view('welcome', ['listItems' => ListItem::all()]);
        // ListItem::all() 代表 SELECT * FROM list_items
        return view('welcome', ['listItems' => ListItem::where('is_complete', 0)->select('id', 'name')->get()]);
    }

    public function saveItem(Request $request) {
        Log::info(json_encode($request->all()));
        // Log::info(json_encode($request->listItem));

        $newListItem = new ListItem;
        $newListItem->name = $request->listItem;
        $newListItem->is_complete = 0;
        $newListItem->save();

        // return "ok";
        // return response()->json(['data' => 'ok']);
        // return view('welcome', ['listItems' => ListItem::all()]);
        return redirect('/');
    }

    public function markComplete($id) {
        $completedItem = ListItem::select('id', 'name')->find($id); // select id,name from list_items where id=id
        $completedItem->is_complete = 1;
        $completedItem->save();

        return redirect('/');
    }
}
