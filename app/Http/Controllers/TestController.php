<?php

namespace App\Http\Controllers;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Models\DomainStatus;
use Illuminate\Support\Facades\Log;

class TestController extends Controller
{
    public function test(Request $request)
    {
        
        // TODO: this !!
        // 假設 $domblock_list 中有一筆資料的 key 是 www.example.com
        $domblock_list = new Collection([
            'www.example.com' => 'value',
            'abc.example.com' => 'sth',
            'play.sboee.com' => 'eee'
            // 其他資料...
        ]);

        // 假設 $dnsquery_data 中有一筆資料的 key 是 example.com
        $dnsquery_data = new Collection([
            'example.com' => 'value',
            '99880.com' => 'wer'
        ]);

        // 使用 filter 保留與 $dnsquery_data 相符的 domain name
        $domblock_list = $domblock_list->filter(function ($value, $key) use ($dnsquery_data) {
            $domainFromSubdomain = explode('.', $key);
            echo json_encode($domainFromSubdomain)."</br>";
            $domainName = $domainFromSubdomain[count($domainFromSubdomain)-2].".".$domainFromSubdomain[count($domainFromSubdomain)-1];
            echo $domainName."</br>";
            return $dnsquery_data->keys()->contains($domainName);
        });

        // 輸出結果
        dd($domblock_list->all());

        $DB_RECORDS = DomainStatus::all();
        // echo "test\n";
        // echo $DB_RECORDS;
        // Log::info($DB_RECORDS);

        return view('test', ['db_data' => $DB_RECORDS]);
    }

    public function isp_mapping(Request $request)
    {

        
        $raw_list = file_get_contents(base_path('isp_list.json'));
        $decoded_raw_list = json_decode($raw_list);
        
        $isp_list = [];
        foreach ($decoded_raw_list as $prop) {
            foreach ($prop as $val) {
                array_push($isp_list, $val);
            }
        }
        Log::info("isp_list: ", $isp_list);

        $DB_RECORDS = DomainStatus::all();
        

        $filter_records = $DB_RECORDS->whereIn('ISP', $isp_list)->where('is_available', 0);

        Log::info("test", $filter_records->toArray());

        // $domblock_list = collect($filter_records)->groupBy('Domain')->map(function ($item) {
        //     $collect_item = collect($item);
        //     $item->block_count = $collect_item->where('is_available', 0)->count();
        //     return $item;
        // })->where('block_count', '>', 0);
        // Log::info(print_r($domblock_list, true));

    }
}

        // $DB_RECORDS_distinct = DomainStatus::select('domain', 'country_code', 'is_available', 'ISP')
        //     ->distinct()
        //     ->where('is_available', '=', 0)
        //     ->whereIn('country_code',['ID', 'TH', 'VN', 'MY'])
        //     ->get();