<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Log;
use stdClass;

class DataProcessController extends Controller
{
    public function domain(Request $request)
    {

        $Domain_pool = ["128coffee.com", "www.128coffee.com", "play.128coffee.com", "241curry.com", "www.241curry.com", 
                        "560bet.com", "580bong.com", "www.580bong.com", "play.580bong.com", "9net88.com"];
        $CountryCode_pool = ["ID", "ID", "VN", "ID", "TH", 
                        "TH", "VN", "VN", "ID", "MY"];
        $ISP_pool = ["Telkomsel", "PT Telkom Indonesia", "Viettel Group", "Telkomsel", "AIS Mobile",
                "DTAC", "MobiFone", "MobiFone", "PT Telkom Indonesia", "U Mobile Sdn Bhd"];

        $mock_result_from_maia = [];
        for ($i = 0; $i < 10; $i++) {
            $temp_obj = new stdClass();
            $temp_obj->Domain = $Domain_pool[$i];
            $temp_obj->CountryCode = $CountryCode_pool[$i];
            $temp_obj->IsAvailable = "0";
            $temp_obj->ISP = $ISP_pool[$i];
            array_push($mock_result_from_maia, $temp_obj);
        }

        // Log::info("mock_result_from_maia: ", $mock_result_from_maia);
        foreach ($mock_result_from_maia as $item) {
            $break_full_domain = explode('.', $item->Domain);
            $MainDomainName = $break_full_domain[count($break_full_domain)-2].".".$break_full_domain[count($break_full_domain)-1];
            $item->MainDomainName = $MainDomainName;
            unset($item->Domain);
            unset($item->ISP);
        }

        // echo json_encode($mock_result_from_maia)."</br>";
        Log::info("mock_result_from_maia: ", $mock_result_from_maia);
        echo "mock_result_from_maia: ".count($mock_result_from_maia)."</br>";
        
        $unique_domain_by_country = collect($mock_result_from_maia)->unique();
        
        Log::info("unique_domain_by_country: ", $unique_domain_by_country->toArray());
        echo "unique_domain_by_country: ".$unique_domain_by_country->count()."</br>";

        echo "==== </br>";
        $domblock_list = $unique_domain_by_country->groupBy('MainDomainName')->map(function ($item) {
            echo "item: ";
            echo $item;
            echo "</br></br>";
            $collect_item = collect($item);
            echo "collect_item: ";
            echo $collect_item;
            echo "</br></br>";
            // $item->block_count = $collect_item->where('IsAvailable', '0')->count();
            $item->block_count = $collect_item->count();
            // $block_count = $collect_item->where('IsAvailable', '0')->count();
            // foreach ($item as $sub_item) {
            //     $sub_item->block_count = $block_count;
            // }
            // echo "modified item: ";
            // echo $item;
            // echo "</br>";
            echo $item->block_count;
            echo "</br></br>";
            echo "----- </br>";
            return $item;
        });

        echo "</br></br></br>";
        echo $domblock_list;

        $domblock_list_2 = $domblock_list->where('block_count', '>', 1);
        echo "</br></br></br>";
        echo $domblock_list_2;
    }
}
