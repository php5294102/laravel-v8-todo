# TODO

Laravel practice
</br>
</br>
Follow https://youtu.be/AEVhR-hD2Wk?si=rCgIhyEhrQzMK2kf

## Create Project
Assign 8.x version
```bash
# composer create-project laravel/laravel <project name> <version>
composer create-project laravel/laravel laravel-v8-todo 8.*
```

## Start Server
```
composer update
composer install
php artisan serve
```

## Clone from git repository
```
cp .env.example .env
php artisan key:generate
```

## Create Migration file
```
php artisan make:migration {name}
```
新增 migration file

## Run Migrate
```
php artisan migrate

```
預設的 table (under `database/migrations`) 如果不用可刪掉，然後跑 `php artisan migrate` 會在 DB 建立相對應的 Table (see Table & Schema under `database/migrations`)

```
php artisan migrate:rollback
```
需要 rollback 時 run rollback


## Create Model
```
php artisan make:model ListItem
```
執行完會在`app/Models` 建立 `ListItem.php`，裡面會是 `ListItem` 這個 class

## Controller
```
php artisan make:controller TodoListController
```
會在 `app/Http/Controllers` 建立 `TodoListController.php`


## Router
在 `routes` folder 中，`web.php` 可以定義 routes (API)

## View
在 `resources/views` folder 中，可讓 controller 回傳 views

## Schedule
```
php artisan schedule:list
```
列出定期要跑的 job
Ref: https://laravel.com/docs/10.x/scheduling

```
php artisan schedule:test
```
A list of scheduled jobs will be shown in the shell, you can select the number to run the job manually.

## Develop Usage
### ide-helper
1. Install
```
composer require --dev barryvdh/laravel-ide-helper
```
2. Register `app/Providers/AppServiceProvider.php` when env is not production
3. Generate IDE Helper
```
php artisan ide-helper:generate
```
4. Reopen IDE

## Tips
```
php artisan make:command ShowMethod --command=show:method
```
Change contents inside the `app/Commands/ShowMethod.php`
what to be output in console is defined in `handle` function

Then execute below
```
php artisan show:method
```
The output will be in the console