<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_status', function (Blueprint $table) {
            $table->increments('id');
            $table->timestampsTz();
            $table->string('domain', 200)->nullable(false);
            $table->string('country_code', 5)->nullable(false);
            $table->boolean('is_available')->nullable(false);
            $table->string('ISP', 200)->nullable(false);
            $table->dateTime('check_time')->nullable(false);
            $table->dateTime('block_time')->nullable(true);
            $table->string('site', 20)->nullable(false);
            $table->string('source', 20)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_status');
    }
}
